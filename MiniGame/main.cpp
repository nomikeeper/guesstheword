/* Created by Nomio 2018 */
#pragma once

#include "Game.h"

Game game; // Instaniating the game

// Entry point of the game
int main() {
	game.Run();
	return 0; // Exit the game
}



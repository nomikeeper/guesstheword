/* Created by Nomio 2018 */
#pragma once
#include <iostream>
#include <string>

class Game {

public:
	void Introduce();		// Introduction of the game
	void Choices();			// Print the player's choices
	void WithMenu();		// Menu section where player choose what he/she want to do
	void WithoutMenu();		// Game plays without a menu
	bool PlayAgain();		// Ask the player if he/she is want to play again
	void SelectGameMode();	// Select the game mode
	void Run();			// Start the game
};
/* Created by Nomio 2018 */
#pragma once

#include <iostream>
#include <string>
using namespace std;


class Gameplay {

public:
	Gameplay();								// Constructor
	~Gameplay();							// Deconstructor
	void startTheGame();					// Start the game

// Private Variables
private:
	const string EnterGuessMessage = "Enter your guess: ";	// Const message
	int TriesLeft = 5;										//Current Try the player have
	int Score = 0;											// Score of the player
	string Guess = "";										// Player's guess
	string GuessHistory = "";								// All of the previous guesses
	string WordHolder = "";									//Word's hidden representation holder
	
//Private Functions
private:
	string GetGuess();							// Get the guess
	void setHiddenWord();						// Set the hidden word for the player
	void calculateTheGuess(string guess);		// Calculate the guess
	void guessMissed();							// When player misses the guess
	void ReportPlayerStatus();					// Report Player Status
	void Play();								// Check Player Status
	void PlayRound();							// Play
	void ResetStatus();								// Reset all stats
	void PrepareWordHolder();					// Prepares the word GUI
	void Congratulate();						// Congratulate the player for winning the game.
	void YouLost();								// You lost message
	bool AddLetterToGuessHistory(char letter);	// Add the newly guessed letter into the guess history
	bool isWordGuessed();						// Check if we won the game
};

struct HiddenWord {
	string Word = "";
	string Hints = "";
};

// Hidden word library
const HiddenWord Libs[] = {
	{
		"Giraffe",
		"Animal, tall, slender",
	},
	{
		"Lion",
		"Animal, Strong, Cat type"
	},
	{
		"Laptop",
		"Fast, Portable, Electonic"
	},
	{
		"Gamefreak",
		"Video game, Company, C++"
	},
	{
		"Gamer",
		"Competitive, Hate losing, Problem solver"
	}
};

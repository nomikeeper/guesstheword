/* Created by Nomio 2018 */
#pragma once

#include "Game.h"
#include "Gameplay.h"
using namespace std;

Gameplay gameplay; // Instantiating the gameplay

// Introduction of the game
void Game::Introduce() {
	constexpr int WORLD_LENGTH = 10;
	// Welcomes the Player
	cout << "Welcome To 'Guess The Word' Game." << endl;
	cout << endl;
	cout << "-------------------------------------" << endl;
	cout << "|            Game tips!             |" << endl;
	cout << "-------------------------------------" << endl;
	cout << "|                                   |" <<endl;
	cout << "| 1) You only have 5 tries to guess |" << endl;
	cout<<	"|    the word.                      |" << endl;
	cout << "| 2) The word has three hints, use  |" << endl;
	cout << "|    them to guess it.              |" << endl;
	cout << "| 3) When you successfully guess a  |" << endl; 
	cout << "|    letter you will gain 1 score.  |" << endl;
	cout << "| 4) When you misses your guess a   |" << endl; 
	cout << "|    letter you will lose 1 score.  |" << endl;
	cout << "| 5) If you can guess the word as   |" << endl;
	cout << "|    whole you will earn 5 scores.  |" << endl;
	cout << "| 6) When you miss guessing the     |" << endl;
	cout << "|    word as whole you will lose.   |" << endl;
	cout << "|                                   |" << endl;
	cout << "-------------------------------------" << endl;
	cout << endl;

	return;
}

// Print the player's choices
void Game::Choices() {
	system("cls");
	cout << "1) Start a game." << endl;
	cout << "9) Close." << endl;
	cout << "Enter your choice: " << endl;
	return;
}

// Menu section where player choose what he/she want to do
void Game::WithMenu() {
	int choice = 0;
	do {
		Choices();	// Print out the choices
		cin >> choice;
		switch (choice)
		{
		case 1: {
			system("cls");
			gameplay.startTheGame();
			break;
		}
		case 9: {
			choice = 9;
			break;
		}
		default: {
			choice = 0;
			break;
		}
		}
	} while (choice != 9);
}

// Game plays without a menu
void Game::WithoutMenu() {
	do {
		system("cls");
		gameplay.startTheGame();
	} while (PlayAgain() != false);
}

// Ask the player if he/she is want to play again
bool Game::PlayAgain() {
	system("cls");
	bool result = true;
	string response = "";	// Holder of the player's input
	cout << "Do you want to play again? [Y/N]" << endl;
	cin >> response;
	system("cls");
	return(response[0] == 'y' || response[0] == 'Y');
}

// Select the game mode
void Game::SelectGameMode() {
	int selection = -1;
	do {
		// Displaying the Menu header
		cout << "-------------------------------------" << endl;
		cout << "|               MENU                |" << endl;
		cout << "-------------------------------------" << endl;
		cout << endl;
		// Print out the game modes
		cout << "1) Press 1 to enable the Menu." << endl;
		cout << "2) Press 2 to disable the Menu." << endl;
		cout << "0) Press 0 to exit." << endl;
		cin >> selection;
		switch (selection)
		{
		case 1: {
				WithMenu();		// Starting game with menu
				break;
			}
		case 2: {
			
				WithoutMenu();	// Starting game without menu 
				break;
			}
		default: 
				break;
		}
	} while (selection != 0);
}

// Start the game
void Game::Run() {
	Introduce();
	SelectGameMode();
}




/* Created by Nomio 2018 */
#pragma once

#include "Gameplay.h"
using namespace std;


HiddenWord* playWord = new HiddenWord;
constexpr int MAX_TRIES = 5;

Gameplay::Gameplay() {
	ResetStatus();
}

Gameplay::~Gameplay() {

}

// Starts a new game 
void Gameplay::startTheGame() {
	ResetStatus();
	system("cls");
	cout << "Tries Left: " << TriesLeft << endl;
	if (TriesLeft == MAX_TRIES) {
		cout << endl;
		cout << "The game has been started" << endl;
	}
	Play();
	return;
}
// Calculate the latest guess
void Gameplay::calculateTheGuess(string guess) {
	if (guess.length() > 1) {
		bool isMatch = false;
		// Check if the guess matches with the word/words
		// Player wins the game
		for (int i = 0; i < playWord->Word.length(); i++) {
			if (playWord->Word[i] != guess[i] && playWord->Word[i] != toupper(guess[i])) {
				TriesLeft = 0;
				break;
			}
			else {
				WordHolder = guess;
				isMatch = true;
			}
		}
		if(isMatch)
			Score += 5;
	}
	else {
		// Check the guessed letter exists in the word/words
		bool existsInHiddenWord = false;								// State of the letter if its exists in hidden word
		bool existsInGuessHistory = AddLetterToGuessHistory(guess[0]);	// State of the letter if its exists in guess history
		for (int i = 0; i < playWord->Word.length(); i++) {
			if (playWord->Word[i] == guess[0] || playWord->Word[i] == toupper(guess[0]) ) {
				WordHolder[i] = guess[0];
				//increase the score of the player
				existsInHiddenWord = true;
				if(existsInGuessHistory == false)
					Score++;
			}
		}
		if (existsInHiddenWord != true) {
			guessMissed();
		}
	}
}

// When player misses the guess
void Gameplay::guessMissed() {
	TriesLeft--;
	Score--;
}


// Get the guess
string Gameplay::GetGuess() {
	// Get a guess from the player
	cout << EnterGuessMessage << endl;
	getline(cin >> ws, Guess);
	return Guess;
}
// Report the player&s status
void Gameplay::ReportPlayerStatus() {
	system("cls");
	if (Guess.length() > 0) {
		// Check if the player guessed a letter or the whole word
		cout << "Your guess was: " << Guess << endl;
		cout << endl;
	}
	// Report how many tries left
	cout << "You have " << TriesLeft << " tries left." << endl;
	cout << "Your score: " << Score << endl;
	cout << "Word's hints: " <<playWord->Hints << endl;
	cout << "Word length: " << playWord->Word.length() << endl;
	cout << "Your guesses: [ " << GuessHistory << " ]" << endl;
	cout << endl;
	cout << "[ " << WordHolder << " ]" << endl;
	cout << endl;
		
}

// play the game
void Gameplay::PlayRound() {
	ReportPlayerStatus();
	auto result = GetGuess();
	calculateTheGuess(result);
}

// Reset all starts
void Gameplay::ResetStatus() {
	TriesLeft = 5;		//Current Try the player have
	Score = 0;			// Score of the player
	Guess = "";			// Player's guess
	GuessHistory = "";	// Resetting the guess History
	WordHolder = "";	//Resetting the word holder
}

// Set the hidden word to guess for the player
void Gameplay::setHiddenWord() {
	auto randNum = rand() % 5; // Rand will return int between 0 to 4 
	*playWord = Libs[randNum]; 
}

// Prepare the hidden words GUI representation
void Gameplay::PrepareWordHolder() {
	for (int i = 0; i < playWord->Word.length(); i++) {
		WordHolder += '*';
	}
}

// Add newly guessed letter into the guess history and return true if
bool Gameplay::AddLetterToGuessHistory(char letter) {
	bool letterExists = false;
	// Check if the letter exist in the guess history
	for (int i = 0; i < GuessHistory.length(); i = i+3) {
		if (GuessHistory[i] == letter)
			letterExists = true;
	}
	// If letter doesn't exist in the guess history add it to it.
	if(letterExists == false)
		GuessHistory = GuessHistory + letter + ", ";

	return letterExists;
}

// Check if we won the game or not
bool Gameplay::isWordGuessed() {
	bool wordGuessed = true;
	for (int i = 0; i < WordHolder.length(); i++) {
		if (WordHolder[i] == '*')
			wordGuessed = false;
	}
	return wordGuessed;
}

// Congratulate the player for winning the game.
void Gameplay::Congratulate() {
	cout << endl;
	cout << "Hidden word was: [ " << playWord->Word << " ]" << endl;
	cout << endl;
	cout << "-------------------------------------" << endl;
	cout << "|                                   |" << endl;
	cout << "|          Congratulations          |" << endl;
	cout << "|                                   |" << endl;
	cout << "|          You won the game!        |" << endl;
	cout << "|                                   |" << endl;
	cout << "|          Your Score: " << Score;
	if (0 < Score && Score < 10) {
		cout << "            |" << endl;
	}
	else if (10 <= Score && Score < 100) {
		cout << "           |" << endl;
	}
	cout << "|                                   |" << endl;
	cout << "-------------------------------------" << endl;
	cout << endl;
	system("pause");
}

// Show the you lost message to the player
void Gameplay::YouLost() {
	cout << endl;
	cout << "-------------------------------------" << endl;
	cout << "|                                   |" << endl;
	cout << "|              Sorry!               |" << endl;
	cout << "|                                   |" << endl;
	cout << "|        You lost in the game!      |" << endl;
	cout << "|                                   |" << endl;
	cout << "-------------------------------------" << endl;
	cout << endl;
	system("pause");
}

// Check the player's status
void Gameplay::Play() {
	bool WordHolderState = false;

	// Setting up a random word from word libs
	setHiddenWord();

	// Preparing the words hidden representation to Player
	PrepareWordHolder();

	// Starting the round
	do {
		PlayRound();
		WordHolderState = isWordGuessed();
		if (TriesLeft <= 0)
			break;
	} while (WordHolderState != true);

	// Checking if the player successfully guessed the whole word or not
	if (WordHolderState)
		Congratulate();
	else
		YouLost();
	ResetStatus();
}
